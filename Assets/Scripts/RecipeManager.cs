using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Vuforia;

public class RecipeManager : MonoBehaviour
{
    [System.Serializable]
    public struct RecipeInstruction
    {
        public string InstructionText;
        public TimelineAsset Animation;
        public ImageTargetBehaviour ImageTarget;
    }

    public RecipeInstruction[] Instructions;
    public Bubble InstructionBubble;
    public Picture RecipePicture;
    public Camera CameraAR;

    private PlayableDirector playableDirector;
    private int instructionIndex = 0;
    public bool recipeCompleted = false;
    public bool waitingPicture = false;

    // Start is called before the first frame update
    void Start()
    {
        playableDirector = GetComponent<PlayableDirector>();
        foreach (RecipeInstruction instruction in Instructions)
        {
            instruction.ImageTarget.enabled = false;
        }
        DisplayInstruction();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            NextInstruction();
        }
    }

    public void DisplayInstruction()
    {
        InstructionBubble.Text = Instructions[instructionIndex].InstructionText;
        InstructionBubble.OnClickEvent.RemoveAllListeners();
        InstructionBubble.OnClickEvent.AddListener(() => NextInstruction());
        Instructions[instructionIndex].ImageTarget.enabled = true;
        playableDirector.time = playableDirector.playableAsset != null ? playableDirector.playableAsset.duration + 0.001d : 0.0d;
        playableDirector.Evaluate();
        playableDirector.Stop();
        playableDirector.time = -0.01d;
        playableDirector.Play(Instructions[instructionIndex].Animation);
        playableDirector.Evaluate();
        InstructionBubble.Initialize();
        InstructionBubble.gameObject.SetActive(true);
    }

    public void NextInstruction()
    {
        if (instructionIndex < Instructions.Length - 1)
        {
            Instructions[instructionIndex].ImageTarget.enabled = false;
            instructionIndex++;
            DisplayInstruction();
            recipeCompleted = false;
        }
        else if (!recipeCompleted && !waitingPicture)
        {
            RecipePicture.gameObject.SetActive(true);
            Instructions[instructionIndex].ImageTarget.enabled = false;
            InstructionBubble.CloseBubble();
            recipeCompleted = true; 
            RecipePicture.gameObject.SetActive(true);
            RecipePicture.OpenPicture();
            waitingPicture = true;
        }
        else if (recipeCompleted && waitingPicture)
        {
            CameraAR.gameObject.SetActive(false);
            RecipePicture.PictureTaken();
            CameraAR.targetTexture = null;
            waitingPicture = false;
        }
    }
}
