using System;
using System.Text;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class VoiceControler : MonoBehaviour
{
    public RecipeManager rm;
    public string[] m_KeyWords = new string[4];
    private KeywordRecognizer recognizer;
    public bool DebugOn;
    
    // Start is called before the first frame update
    void Start()
    {
        DebugOn = false;
        
        rm = GetComponent<RecipeManager>();
        m_KeyWords[0] = "next";     // next step !
        m_KeyWords[1] = "suivante";  // étape suivante
        m_KeyWords[2] = "prochaine";// prochaine étape
        m_KeyWords[3] = "bug";    // Ok sa merde lance debug
        
        recognizer = new KeywordRecognizer(m_KeyWords); // on file au Reconizer le tab des Key Words
        recognizer.OnPhraseRecognized += OnPhraseRecognized; // fonction appeler quand sa trigger un mot cles
        recognizer.Start();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        // par exemple quand il 'entend' left il lance moveLeft()
        if (args.text == "next" || args.text == "suivante" || args.text == "prochaine")
        {
            // Medium or High
            // if (args.confidence <= ConfidenceLevel.Medium)
            // {
                rm.NextInstruction();
            // }
            if (DebugOn)
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendFormat("{0} ({1}){2}", args.text, args.confidence, Environment.NewLine);
                builder.AppendFormat("\tTimestamp: {0}{1}", args.phraseStartTime, Environment.NewLine);
                builder.AppendFormat("\tDuration: {0} seconds{1}", args.phraseDuration.TotalSeconds, Environment.NewLine);
                Debug.Log(builder.ToString()); // affiche les info de args
            }
        }

        if (args.text == "bug")
        {
            DebugOn = !DebugOn;
            Debug.Log("Debug mode switch");
        }

    }
}
