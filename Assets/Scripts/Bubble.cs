using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Bubble : MonoBehaviour
{
    public UnityEvent OnClickEvent;
    public string Text;

    private TextMeshProUGUI bubbleText;
    private Button button;
    private float lastClickTime = 0.0f;
    private bool isClosed = false;

    // Start is called before the first frame update
    void Start()
    {
        button = transform.Find("Button").GetComponent<Button>();
        button.onClick.AddListener(() =>
        {
            if ((Time.time - lastClickTime) > 0.5f)
            {
                lastClickTime = Time.time;
                OnClickEvent.Invoke();
            }

        });
        Initialize();
    }

    public void Initialize()
    {
        bubbleText = transform.Find("Background").Find("BubbleText").GetComponent<TextMeshProUGUI>();
        if (!string.IsNullOrWhiteSpace(Text))
        {
            bubbleText.text = Text;
        }
    }

    public void CloseBubble()
    {
        if (isClosed)
        {
            return;
        }
        GetComponent<Animator>().Play("BubbleClose");
        isClosed = true;
        // gameObject.SetActive(false);
    }
}
