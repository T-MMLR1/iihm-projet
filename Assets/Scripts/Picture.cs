using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Picture : MonoBehaviour
{
    private bool isOpened = false;

    private void Awake()
    {
        gameObject.SetActive(true);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OpenPicture()
    {
        if (isOpened)
        {
            return;
        }
        GetComponent<Animator>().Play("PictureOpen");
        isOpened = true;
        WebCamDevice[] devices = WebCamTexture.devices;
        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log(devices[i].name);
        }
    }

    public void PictureTaken()
    {
        if (!isOpened)
        {
            return;
        }
        GetComponent<Animator>().Play("PictureTaken");
    }
}
